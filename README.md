```bash
docker-compose exec app php artisan jwt:secret
```


### Registration

```bash
curl -D - -d '{"name":"alice", "password":"1234", "email": "alice@test.com"}' -H "Content-Type: application/json" -X POST http://localhost:8000/api/auth/register \
&& curl -D - -d '{"name":"bob", "password":"1234", "email": "bob@test.com"}' -H "Content-Type: application/json" -X POST http://localhost:8000/api/auth/register \
&& curl -D - -d '{"name":"eve", "password":"1234", "email": "eve@test.com"}' -H "Content-Type: application/json" -X POST http://localhost:8000/api/auth/register
```


### Login

```bash
curl -D - -d '{"email":"alice@test.com", "password":"1234"}' -H "Content-Type: application/json" -X POST http://localhost:8000/api/auth/login
```


### User

```bash
curl -D - -H "Authorization: Bearer TOKEN" -H "Content-Type: application/json" -X GET http://localhost:8000/api/auth/user
```


### Deal

```bash
curl -D - -d '{"amount":"1.01", "recipients_ids": [2, 3]}' -H "Authorization: Bearer TOKEN" -H "Content-Type: application/json" -X POST http://localhost:8000/api/pay/deal
```


### Operations

```bash
curl -D - -H "Authorization: Bearer TOKEN" -H "Content-Type: application/json" -X GET http://localhost:8000/api/info/operations
```


### Balance history

```bash
curl -D - -H "Authorization: Bearer TOKEN" -H "Content-Type: application/json" -X GET http://localhost:8000/api/info/balance-history
```
