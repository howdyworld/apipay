<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'prefix' => 'auth'

], function ($router) {

    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
});

Route::group([

    'middleware' => 'auth.jwt', // sanctung
    'prefix' => 'pay'

], function ($router) {

    Route::post('deal', 'PayController@deal');
});

Route::group([

    'middleware' => 'auth.jwt',
    'prefix' => 'info'

], function ($router) {

    Route::get('operations', 'InfoController@userOperations');
    Route::get('balance-history', 'InfoController@userBalanceHistory');
});


Route::group([

    'middleware' => 'auth.jwt',
    'prefix' => 'schedule'

], function ($router) {

    Route::post('schedule-pay', 'ScheduledPayController@schedulePay');
});
