<?php

declare(strict_types=1);

namespace App\Apipay\Repositories;

use Exception;
use Illuminate\Support\Facades\DB;
use App\User;
use App\BalanceHistory;
use App\Transaction;
use App\Exceptions\PayException;
use App\Apipay\Enums\PayResultEnum;

class PayRepository
{
    public const TRANSACTION_RETRY_LIMIT = 3;

    private function processSenderPayment(
        int $senderId,
        string $senderTotalAmount
    ) {
        $previousSenderBalance = (
            User::where('id', $senderId)->value('balance')
        );
        $isSenderBalanceUpdated = User::where('id', $senderId)
            ->where('balance', '>=', $senderTotalAmount)
            ->decrement('balance', $senderTotalAmount);
        if (!$isSenderBalanceUpdated) {
            throw new PayException(
                'Not enough money', PayResultEnum::ERR_CODE_NOT_ENOUGH_MONEY
            );
        }
        BalanceHistory::create([
            'user_id' => $senderId,
            'previous_balance' => $previousSenderBalance,
            'current_balance' => bcsub(
                $previousSenderBalance, $senderTotalAmount, 2
            ),
            'created_at' => BalanceHistory::getTimestampDatetime(),
        ]);
    }

    private function processRecipientsPayment(
        int $senderId,
        array $recipientsAmountsMap
    ) {
        $recipientsIds = array_keys($recipientsAmountsMap);
        $previousRecipientsStates = User::select('id', 'balance')
            ->whereIn('id', $recipientsIds)
            ->get();

        foreach ($recipientsIds as $recipientId) {
            $recipientAmount = $recipientsAmountsMap[$recipientId];
            User::where('id', $recipientId)
                ->increment('balance', $recipientAmount);
        }

        $recipientsPreviousBalances = [];
        foreach ($previousRecipientsStates as $recipientState) {
            $recipientsPreviousBalances[$recipientState->id] = (
                $recipientState->balance
            );
        }
        $transactions = [];
        $recipientsBalancesHistory = [];
        foreach ($recipientsIds as $recipientId) {
            $recipientAmount = $recipientsAmountsMap[$recipientId];
            $transactions[] = [
                'from_user_id' => $senderId,
                'to_user_id' => $recipientId,
                'amount' => $recipientAmount,
                'created_at' => Transaction::getTimestampDatetime(),
            ];
            $previousRecipientBalance = (
                $recipientsPreviousBalances[$recipientId]
            );
            $recipientsBalancesHistory[] = [
                'user_id' => $recipientId,
                'previous_balance' => $previousRecipientBalance,
                'current_balance' => bcadd(
                    $previousRecipientBalance, $recipientAmount, 2
                ),
                'created_at' => BalanceHistory::getTimestampDatetime(),
            ];
        }
        Transaction::insert($transactions);
        BalanceHistory::insert($recipientsBalancesHistory);
    }

    public function doPayWithoutTransaction(
        int $senderId,
        array $recipientsAmountsMap
    ) {
        $recipientsIds = array_keys($recipientsAmountsMap);
        $allUsersIdsInTransaction = array_merge($recipientsIds, [$senderId]);
        User::whereIn('id', $allUsersIdsInTransaction)->lockForUpdate();

        $senderTotalAmount = '0.00';
        foreach ($recipientsAmountsMap as $recipientAmount) {
            $senderTotalAmount = bcadd($senderTotalAmount, $recipientAmount, 2);
        }
        $this->processSenderPayment($senderId, $senderTotalAmount);
        $this->processRecipientsPayment($senderId, $recipientsAmountsMap);
    }

    public function doDealTransaction($senderId, $recipientsAmountsMap)
    {
        DB::beginTransaction();
        $this->doPayWithoutTransaction($senderId, $recipientsAmountsMap);
        DB::commit();
    }

    public function doRetriablePayTransaction(
        int $senderId,
        array $recipientsAmountsMap
    ): int {
        $resultCode = PayResultEnum::ERR_CODE_INTERNAL;
        for (
            $attempt = 0;
            $attempt < PayRepository::TRANSACTION_RETRY_LIMIT;
            $attempt++
        ) {
            try {
                $this->doDealTransaction($senderId, $recipientsAmountsMap);
                return PayResultEnum::PAYMENT_OK;
            } catch (PayException $e) {
                DB::rollBack();
                return $e->getCode();
            } catch (Exception $e) {
                DB::rollBack();
            }
        }
        return $resultCode;
    }
}
