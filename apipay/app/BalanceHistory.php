<?php

namespace App;

use App\BaseModel;

class BalanceHistory extends BaseModel
{
    public $table = "balance_history";

    protected $fillable = [
        'user_id', 'previous_balance', 'current_balance',
    ];
}
