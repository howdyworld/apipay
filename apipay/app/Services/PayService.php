<?php

declare(strict_types=1);

namespace App\Services;

use App\Apipay\Repositories\PayRepository;

class PayService
{
    public function __construct(PayRepository $payRepository)
    {
        $this->payRepository = $payRepository;
    }

    public function dealMany(
        int $senderId,
        array $recipientsIds,
        string $amount
    ): int {
        $recipientsAmountsMap = [];
        foreach ($recipientsIds as $recipientId) {
            $recipientsAmountsMap[$recipientId] = $amount;
        }
        $resultCode = $this->payRepository->doRetriablePayTransaction(
            $senderId, $recipientsAmountsMap
        );
        return $resultCode;
    }
}
