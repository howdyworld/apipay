<?php

namespace App;

use App\BaseModel;

class PaymentScheduleRecipients extends BaseModel
{
    protected $fillable = [
        'recipient_id', 'payment_schedule_id', 'amount'
    ];
}
