<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\DealRequest;
use JWTAuth;
use Illuminate\Http\JsonResponse;
use App\Apipay\Enums\PayResultEnum;
use App\Services\PayService;


class PayController extends Controller
{
    public function __construct(PayService $payService)
    {
        $this->user = JWTAuth::parseToken()->authenticate();
        $this->payService = $payService;
    }

    public function deal(DealRequest $request): JsonResponse
    {
        $errors = $request->validateRequestData($this->user);
        if (!empty($errors)) {
            return response()->json($errors, 400);
        }
        $transactionData = $request->json()->all();
        $recipientsIds = $transactionData['recipients_ids'];
        $amount = $transactionData['amount'];
        $resultCode = $this->payService->dealMany(
            $this->user->id,
            $recipientsIds,
            $amount
        );
        switch ($resultCode) {
            case PayResultEnum::PAYMENT_OK:
                return response()->json(['success' => 'success'], 200);
            case PayResultEnum::ERR_CODE_INTERNAL:
                return response()->json(
                    ['errors' => ['try again later']], 500
                );
            case PayResultEnum::ERR_CODE_NOT_ENOUGH_MONEY:
                return response()->json(
                    ['errors' => ['not enough money']], 403
                );
        }
        return response()->json(['errors' => ['internal error']], 500);
    }
}
