<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    /**
     * @param \App\Http\Requests\RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $payload = $request->json()->all();
        $payload['password'] = Hash::make($payload['password']);
        $user = User::create($payload);
        $token = auth()->login($user);

        return $this->respondWithToken($token);
    }

    /**
     * @param \App\Http\Requests\LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        if (!$token = auth()->attempt($request->json()->all())) {
            return response()->json([
                'message' => 'Invalid username or password.'], 401
            );
        }

        return $this->respondWithToken($token);
    }

    /**
     * @param string $token
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken(string $token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'valid_until' => auth()->factory()->getTTL() * 60,
        ]);
    }
}
