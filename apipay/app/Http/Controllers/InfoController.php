<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use JWTAuth;
use App\Transaction;
use App\BalanceHistory;


class InfoController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function userOperations()
    {
        $transactions = Transaction::select(
            'from_user_id', 'to_user_id', 'amount', 'created_at'
        )
            ->where('from_user_id', $this->user->id)
            ->orWhere('to_user_id', $this->user->id)
            ->orderBy('created_at', 'DESC')
            ->limit(10)
            ->get()
            ->toArray();
        return response()->json([ 'transactions' => $transactions ], 200);
    }

    public function userBalanceHistory()
    {
        $balanceHistory = BalanceHistory::select(
            'previous_balance', 'current_balance', 'created_at'
        )
            ->where('user_id', $this->user->id)
            ->orderBy('created_at', 'DESC')
            ->limit(10)
            ->get()
            ->toArray();
        return response()->json([ 'balance_history' => $balanceHistory ]);
    }
}
