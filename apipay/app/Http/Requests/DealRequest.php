<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Http\Requests\BaseApiRequest;
use App\User;

class DealRequest extends BaseApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|regex:/^[0-9]+(?:\.[0-9]{1,2})?$/',
            'recipients_ids' => 'required|array',
            'recipients_ids.*' => 'integer',
        ];
    }

    public function validateRequestData($user)
    {
        $transactionData = $this->json()->all();
        $recipientsIds = $transactionData['recipients_ids'];
        if (in_array($user->id, $recipientsIds)) {
            return [ 'errors' => ['sender_id in recipient_ids'] ];
        }
        $dbRecipientsCount = User::whereIn('id', $recipientsIds)->count();
        if ($dbRecipientsCount != count($recipientsIds)) {
            return [ 'errors' => ['Some of recipients ids not found'] ];
        }
        return [];
    }
}
