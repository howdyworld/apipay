<?php

namespace App;

use App\BaseModel;
use App\PaymentScheduleRecipients;

class PaymentSchedule extends BaseModel
{
    protected $fillable = [
        'start_date', 'end_date', 'description', 'is_active',
    ];

    public function recipients()
    {
        return $this->hasMany(PaymentScheduleRecipients::class);
    }
}
