<?php

namespace App;

use App\BaseModel;

class Transaction extends BaseModel
{
    protected $fillable = [
        'from_user_id', 'to_user_id', 'amount',
    ];
}
