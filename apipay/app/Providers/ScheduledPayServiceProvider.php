<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Apipay\Repositories\PayRepository;
use App\Services\ScheduledPayService;

class ScheduledPayServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\ScheduledPayService', function ($app) {
            return new ScheduledPayService(new PayRepository());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
