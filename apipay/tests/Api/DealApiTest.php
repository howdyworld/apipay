<?php

namespace Tests\Feature\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DealApiTest extends TestCase
{
    protected $senderName;
    protected $senderEmail;
    protected $senderPassword;
    protected $recipientName;
    protected $recipientEmail;

    public function setUp(): void
    {
        parent::setUp();
        $this->senderName = 'alice';
        $this->senderEmail = $this->senderName. '@test.com';
        $this->senderPassword = '1234';
        $this->recipientName = 'bob';
        $this->recipientEmail = $this->recipientName. '@test.com';
    }

    private function createSenderUser($senderBalance)
    {
        factory(User::class)->create([
            'name' => $this->senderName,
            'email' => $this->senderEmail,
            'password' => bcrypt($this->senderPassword),
            'balance' => $senderBalance,
        ]);
        $loginApiResponse = $this->json(
            'POST',
            'api/auth/login',
            ['email' => $this->senderEmail, 'password' => $this->senderPassword]
        );
        $loginApiResponse->assertStatus(200);
        $this->assertTrue(isset($loginApiResponse['access_token']));
        return $loginApiResponse['access_token'];
    }

    private function createRecipientUser($recipientBalance)
    {
        return factory(User::class)->create([
            'name' => $this->recipientName,
            'email' => $this->recipientEmail,
            'balance' => $recipientBalance,
        ]);
    }

    private function doTransaction($senderBalance, $recipientBalance, $sendAmount)
    {
        $senderToken = $this->createSenderUser($senderBalance);
        $recipientUser = $this->createRecipientUser($recipientBalance);
        $dealApiResponse = $this->json(
            'POST',
            'api/pay/deal',
            [
                'amount' => $sendAmount,
                'recipients_ids' => [$recipientUser->id]
            ],
            ['Authorization' => 'Bearer '. $senderToken]
        );
        return $dealApiResponse;
    }

    private function checkBalances($senderBalance, $recipientBalance)
    {
        $recipientAfterTransaction = User::whereEmail(
            $this->recipientEmail
        )->first();
        $this->assertEquals(
            $recipientBalance,
            $recipientAfterTransaction->balance
        );

        $senderAfterTransaction = User::whereEmail($this->senderEmail)->first();
        $this->assertEquals(
            $senderBalance, $senderAfterTransaction->balance
        );
    }

    public function testSuccessfulTransaction()
    {
        $senderBalance = '100.00';
        $recipientBalance = '0.00';
        $sendAmount = '50.50';
        $dealApiResponse = $this->doTransaction(
            $senderBalance, $recipientBalance, $sendAmount
        );
        $dealApiResponse->assertStatus(200);

        $senderRemainingBalance = bcsub($senderBalance, $sendAmount, 2);
        $recipientBalanceAfterTransaction = bcadd(
            $recipientBalance, $sendAmount, 2
        );
        $this->checkBalances(
            $senderRemainingBalance,
            $recipientBalanceAfterTransaction,
        );
    }

    public function testNotEnoughMoney()
    {
        $senderBalance = '100.00';
        $recipientBalance = '0.00';
        $sendAmount = '100.01';
        $dealApiResponse = $this->doTransaction(
            $senderBalance, $recipientBalance, $sendAmount
        );
        $dealApiResponse->assertStatus(403);
        $this->checkBalances($senderBalance, $recipientBalance);
    }

    public function testNegativeSendAmount()
    {
        $senderBalance = '100.00';
        $recipientBalance = '0.00';
        $sendAmount = '-50.50';
        $dealApiResponse = $this->doTransaction(
            $senderBalance, $recipientBalance, $sendAmount
        );
        $dealApiResponse->assertStatus(422);
        $this->checkBalances($senderBalance, $recipientBalance);
    }

    public function testNotExistsRecipient()
    {
        $senderToken = $this->createSenderUser('100.00');
        $this->json(
            'POST',
            'api/pay/deal',
            [
                'amount' => '100.0',
                'recipients_ids' => [-1]
            ],
            ['Authorization' => 'Bearer '. $senderToken]
        )
            ->assertStatus(400);
    }

    public function testWithoutAmount()
    {
        $senderToken = $this->createSenderUser('100.00');
        $recipientUser = $this->createRecipientUser('0.00');
        $this->json(
            'POST',
            'api/pay/deal',
            [
                'recipients_ids' => [$recipientUser->id]
            ],
            ['Authorization' => 'Bearer '. $senderToken]
        )
            ->assertStatus(422);
    }

    public function testWithoutRecipients()
    {
        $senderToken = $this->createSenderUser('100.00');
        $this->json(
            'POST',
            'api/pay/deal',
            [
                'amount' => '100.0',
            ],
            ['Authorization' => 'Bearer '. $senderToken]
        )
            ->assertStatus(422);
    }

    public function testWithoutAmountAndRecipients()
    {
        $senderToken = $this->createSenderUser('100.00');
        $this->json(
            'POST',
            'api/pay/deal',
            [],
            ['Authorization' => 'Bearer '. $senderToken]
        )
            ->assertStatus(422);
    }

    public function testSenderInRecipients()
    {
        $sender = factory(User::class)->create([
            'name' => $this->senderName,
            'email' => $this->senderEmail,
            'password' => bcrypt($this->senderPassword),
            'balance' => '100.00',
        ]);
        $loginApiResponse = $this->json(
            'POST',
            'api/auth/login',
            ['email' => $this->senderEmail, 'password' => $this->senderPassword]
        );
        $loginApiResponse->assertStatus(200);
        $this->assertTrue(isset($loginApiResponse['access_token']));
        $senderToken = $loginApiResponse['access_token'];
        $this->json(
            'POST',
            'api/pay/deal',
            [
                'amount' => '100.0',
                'recipients_ids' => [$sender->id]
            ],
            ['Authorization' => 'Bearer '. $senderToken]
        )
            ->assertStatus(400);
    }
}
