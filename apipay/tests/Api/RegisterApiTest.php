<?php

namespace Tests\Feature\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSuccessfulRegister()
    {
        $name = 'alice';
        $email = $name .'@test.com';
        $password = '1234';
        $apiResponse = $this->json(
            'POST', 'api/auth/register',
            ['name' => $name, 'password' => $password, 'email' => $email]
        );
        $apiResponse->assertStatus(200);
        $this->assertTrue(isset($apiResponse['access_token']));
        $createdUser = User::whereEmail($email)->first();
        $this->assertEquals($name, $createdUser->name);
        $this->assertEquals($email, $createdUser->email);
    }

    public function testFailRegisterShortName()
    {
        $name = 'a';
        $email = $name .'@test.com';
        $password = '1234';
        $apiResponse = $this->json(
            'POST', 'api/auth/register',
            ['name' => $name, 'password' => $password, 'email' => $email]
        );
        $apiResponse->assertStatus(422);
    }

    public function testFailRegisterEmailAlreadyExists()
    {
        $name = 'alice';
        $email = $name .'@test.com';
        $password = '1234';
        factory(User::class)->create([
            'name' => $name,
            'password' => bcrypt($password),
            'email' => $email,
            'balance' => '0.0',
        ]);
        $apiResponse = $this->json(
            'POST', 'api/auth/register',
            ['name' => $name, 'password' => $password, 'email' => $email]
        );
        $apiResponse->assertStatus(422);
    }
}
