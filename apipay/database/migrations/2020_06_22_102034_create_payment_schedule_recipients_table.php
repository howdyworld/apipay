<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentScheduleRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_schedule_recipients', function (Blueprint $table) {
            $table->id();
            $table->foreignId('recipient_id');
            $table->foreignId('payment_schedule_id');
            $table->decimal('amount', 10, 2);

            $table->foreign('recipient_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('payment_schedule_id')
                ->references('id')
                ->on('payment_schedules')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_schedule_recipients');
    }
}
