
insert into payment_schedules (sender_id, description, days_frequency, from_date, last_payment_date)
values (1, 'pay every day test', 1, '2020-06-18 17:36:40', '2020-06-18 17:36:40');


insert into payment_schedule_recipients (recipient_id, payment_schedule_id, amount)
values (2, 1, '1.02');

insert into payment_schedule_recipients (recipient_id, payment_schedule_id, amount)
values (3, 1, '2.03');

update payment_schedules set from_date='2020-06-18 17:36:40', last_payment_date='2020-06-18 17:36:40';
